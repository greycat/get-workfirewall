<#
.SYNOPSIS
Returns the status of Windows firewalls on the specified computer.
.DESCRIPTION
Returns the status of the Windows Defender Firewall. If the firewall is on. Which profiles are enabled or disabled. The condition and startup type of the service. Online True/False refers to the state of computer after a Test-Connection check.
.PARAMETER ComputerName
The computername or names to query
.EXAMPLE
Get-WorkFirewall -ComputerName localhost
Name             : localhost
Online           : True
ServiceStatus    : Running
ServiceStartType : Automatic
DomainProfile    : True
PrivateProfile   : True
PublicProfile    : True
OS               : Microsoft Windows 10 Enterprise Evaluation
.EXAMPLE
C:\PS>$computers = Get-ADComputer -Filter 'Name -like "*" -and Enabled -eq "True"'
C:\PS>Get-WorkFirewall -ComputerName ($computers).name | Format-Table

Name       Online ServiceStatus ServiceStartType DomainProfile PrivateProfile PublicProfile OS
----       ------ ------------- ---------------- ------------- -------------- ------------- --
DC00         True       Running        Automatic          True           True          True Microsoft Windows Server...
W10-01       True       Running        Automatic          True           True          True Microsoft Windows 10 Ent...
W10-02      False
Storage01   False
PME_SERVER  False
W10-03      False
SQLTEST      True       Running        Automatic          True           True          True Microsoft Windows Server...
TEST2016    False
IIS01       False
HV          False
TPMSERVER   False
.INPUTS
System.String
.OUTPUTS
System.Management.Automation.PSCustomObject
#>

function Get-WorkFirewall {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline = $True, Mandatory = $true)]
        $ComputerName
    )#end param block
    begin {
        $Output = @()
        $Count = $ComputerName.Count
        $ProgressSplat = @{
            Activity = 'Activity: Checking firewall'
            CurrentOperation = "Current Operation: Begin block"
            PercentComplete = 0
        }
        Write-Progress @ProgressSplat
        $progress = 0
    }#end begin
    process {
        foreach ($computer in $computername) {
            # Update $ProgressSplat
            $progress++
            [int]$percentage = ($progress / $Count) * 100
            $ProgressSplat.CurrentOperation = "Current Operation: $computer"
            $ProgressSplat.PercentComplete = $percentage
            Write-Progress @ProgressSplat

            $Object = @()

            $TestConnectionResult = Test-Connection $computer -Quiet -Count 1
            if ($TestConnectionResult) {
                $session = New-CimSession -ComputerName $computer
                $Firewallprofile = Get-NetFirewallProfile -CimSession $session
                $Object = [PSCustomObject]@{
                    Name             = $Computer
                    Online           = $TestConnectionResult
                    ServiceStatus    = (Get-Service -ComputerName $computer -Name mpssvc).Status
                    ServiceStartType = (Get-Service -ComputerName $computer -Name mpssvc).StartType
                    DomainProfile    = $Firewallprofile | Where-Object -Property Name -eq Domain | Select-Object -ExpandProperty Enabled
                    PrivateProfile   = $Firewallprofile | Where-Object -Property Name -eq Private | Select-Object -ExpandProperty Enabled
                    PublicProfile    = $Firewallprofile | Where-Object -Property Name -eq Public | Select-Object -ExpandProperty Enabled
                }#end PSCustomObject
                $Output += $Object
            }#end if block
            else {
                $Object = [PSCustomObject]@{
                    Name             = $Computer
                    Online           = $TestConnectionResult
                    ServiceStatus    = $null
                    ServiceStartType = $null
                    DomainProfile    = $null
                    PrivateProfile   = $null
                    PublicProfile    = $null
                }#end PSCustomObject
                $Output += $Object
            }#end else block
            Get-CimSession | Remove-CimSession
        }#end foreach
    }#end process
    end {
        $Output
    }#end end
}#end function
